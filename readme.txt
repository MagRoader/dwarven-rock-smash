Dwarven Rock Smash
by Colin C.
magroader@gmail.com

Created for Ludum Dare 15 - "Caverns"
    http://www.ludumdare.com/



** NOTE TO LUDUM DARE VOTERS **

v1 of Dwarven Rock Smash was my original submission.  I have since modified the game
to add Leaderboards (and nothing else).  I thought this would too valuable of an
addition to the game, even though it's probably considered cheating.

When determining scoring for judging purposes, please disregard the Leaderboards
capabilities of Dwarven Rock Smash.  Enjoy!



Technology used to make this game:

    Flex                - http://www.adobe.com/products/flex/
    Flixel              - http://flixel.org/
    FlashDevelop        - http://www.flashdevelop.org/
    Paint.NET           - http://www.getpaint.net/
    sfxr                - http://www.cyd.liu.se/~tompe573/hp/project_sfxr.html
    LAME MP3 Encoder    - http://lame.sourceforge.net/
    Mochi API           - https://www.mochimedia.com/



Flixel's source and license are included under \lib\flixel_v1.25.

Modifications to Flixel were made, including:
* A few minor bug fixes
* Slight changes to default menu behavior
* Removal of the "Mode" sample game



Mochi API's source and license are uncluded under \lib\MochiAPI_v3_3

Modifications to Mochi API were made, including:
* Removed unused AS2 version of code.
* Removed unused examples.



With the exceptions of Flixel and Mochi API, all parts of Dwarven Rock Smash
are in the Public Domain. To view a copy of the public domain certification,
visit http://creativecommons.org/licenses/publicdomain/ or send a letter to:
    Creative Commons
    171 Second Street, Suite 300
    San Francisco, California  94105
    USA