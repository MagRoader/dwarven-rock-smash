﻿package
{
    import com.adamatomic.flixel.FlxState;
    import flash.display.MovieClip;
    import mochi.as3.MochiServices;

    /// Just a bunch of helpful global functions
    public class GameGlobal
    {
        /// Whether we have ever connected to the leaderboard
        public static var ConnectedToLeaderboard:Boolean = false;

        /// Return a random number between the two provided numbers
        public static function RandomBetween(min:Number, max:Number, whole:Boolean = true):Number
        {
            var num:Number = Math.random() * ((max-min) + (whole ? 1 : 0)) + min;
            return (whole ? Math.floor(num) : num);
        }

        /// Return the ID of the Mochi Leaderboard
        public static function GetLeaderboardId():String
        {
            var o:Object = { n: [15, 11, 2, 5, 5, 8, 3, 0, 15, 8, 10, 10, 13, 8, 14, 9], f: function (i:Number,s:String):String { if (s.length == 16) return s; return this.f(i+1,s + this.n[i].toString(16));}};
            return o.f(0,"");
        }

        /// Safely connect to the Mochi leaderoard
        public static function ConnectToLeaderboard(state:FlxState):void
        {
            if (!ConnectedToLeaderboard)
            {
                var leaderboardMovie:MovieClip = state.parent.addChild(new MovieClip) as MovieClip;

                //var leaderboardMovie:MovieClip = addChild(new MovieClip()) as MovieClip;
                MochiServices.connect("dedd4ad83aa04e7b", leaderboardMovie);
                ConnectedToLeaderboard = true;
            }
        }

    }

}