﻿package spawner 
{
    import flash.geom.Point;
    
    /// Class (more like a struct really) to house info about a Rock spawn
    public class RockPatternRockData
    {
        /// Time to wait before firing our next rock
        public var TicksToNext:Number;
        
        /// x/y velocity of the rock we want to spawn
        public var Velocity:Point;
        
        /// Offset from center where we want to fire this off
        public var Offset:Point;
        
        /// Constructor
        public function RockPatternRockData(ticksToNext:Number = 0, velocityX:Number = 0, velocityY:Number = 100, offsetX:Number = 0, offsetY:Number = 0)
        {
            TicksToNext = ticksToNext;
            Velocity = new Point(velocityX, velocityY);
            Offset = new Point(offsetX, offsetY);
        }
    }    
}