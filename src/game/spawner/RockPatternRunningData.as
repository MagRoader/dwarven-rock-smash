﻿package spawner 
{
    import flash.geom.Point;
    
    /// Struct tracking our current tick and how far into an 
    /// array of rock data we are at the moment
    public class RockPatternRunningData
    {
        /// Ticks before we increment our current index
        public var TicksRemaining:Number = 0;
        
        /// Current index
        public var Index:Number = 0;
        
        /// The offset X,Y position for where this is firing from
        public var Offset:Point = new Point(0, 0);
        
        /// COnstructor
        public function RockPatternRunningData(offset:Point)
        {
            Offset = offset;
        }
    }    
}