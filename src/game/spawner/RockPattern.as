﻿package spawner
{
    import character.Rock;
    import com.adamatomic.flixel.FlxCore;
    import flash.geom.Point;
    import flash.geom.Rectangle;


    /// Base class for a rock spawn pattern
    public class RockPattern extends FlxCore
    {
        /// A bunch of RockPatternRockData objects for this pattern, each signifying a rock
        private var mPatternArray/*RockPatternRockData*/:Array;

        /// Array of indices into the above mPattern Array
        private var mPatternRunData:/*RockPatternRunningData*/Array;

        /// Function we will call to get a new rock
        private var mRockAddFunction:Function;

        /// This is a bounding box of all the possible offsets we have been
        /// given.  We use this to make sure we don't start a pattern in an
        /// invalid location (like, too far to the left or something).
        private var mOffsetBounds:Rectangle;


        /// Constructor
        public function RockPattern(getNewRockFunction:Function):void
        {
            mPatternArray = new Array();
            mPatternRunData = new Array();
            mOffsetBounds = new Rectangle(0, 0, 0, 0);
            mRockAddFunction = getNewRockFunction;
        }

        /// Start this thing up so it starts firing rocks everywhere
        /// like a crazy person who has a bunch of rocks.  And a rock gun.
        public function Fire(startingArea:Rectangle):void
        {
            if (startingArea.width >= mOffsetBounds.width &&
                startingArea.height >= mOffsetBounds.height)
            {

                // Shift the starting area by our possible offsets so we don't
                // start a rock in a weird place, like offscreen
                startingArea.left = startingArea.left - mOffsetBounds.left;
                startingArea.right = startingArea.right - mOffsetBounds.right;
                startingArea.top = startingArea.top - mOffsetBounds.top;
                startingArea.bottom = startingArea.bottom - mOffsetBounds.bottom;

                mPatternRunData.push(new RockPatternRunningData(new Point(
                    GameGlobal.RandomBetween(startingArea.left, startingArea.right),
                    GameGlobal.RandomBetween(startingArea.top, startingArea.bottom))));
            }
            else
            {
                // Couldn't fire this - not enough room
                throw "Not enough area to fire a rock";
            }
        }

        /// Remove everything we put in here with Fire
        public function Douse():void
        {
            mPatternRunData = new Array();
        }

        /// Wrapper around AddPatternData
        /// Add some data for a new rock pattern.  Call this a bunch of times
        /// with cool rock patterns
        public function Add(ticksToNext:Number = 0, velocityX:Number = 0, velocityY:Number = 100, offsetX:Number = 0, offsetY:Number = 0):void
        {
            AddPatternData(new RockPatternRockData(ticksToNext, velocityX, velocityY, offsetX, offsetY));
        }

        /// Add a new entry to the pattern data. Should be called several times in a row to add a bunch of rock
        /// pattern data; the rest will be taken care of for us
        public function AddPatternData(data:RockPatternRockData):void
        {
            mPatternArray.push(data);

            // Track how much offset area we can possibly take up
            mOffsetBounds.left      = Math.min(mOffsetBounds.left, data.Offset.x);
            mOffsetBounds.right     = Math.max(mOffsetBounds.right, data.Offset.x);
            mOffsetBounds.top       = Math.min(mOffsetBounds.top, data.Offset.y);
            mOffsetBounds.bottom    = Math.max(mOffsetBounds.bottom, data.Offset.y);
        }

        /// Update override so we can fire rocks everywhere
        override public function update():void
        {
            // Loop through all the currently running patterns
            for (var i:Number = 0 ; i < mPatternRunData.length ; ++i)
            {
                var currentRunData:RockPatternRunningData = mPatternRunData[i];
                if (currentRunData.Index < mPatternArray.length)
                {
                    currentRunData.TicksRemaining--;

                    // Keep firing rocks while we have 0 ticks remaining - some rocks could fire simultaneously, you see
                    while (0 >= currentRunData.TicksRemaining && currentRunData.Index < mPatternArray.length)
                    {
                        _FireARock(mPatternArray[currentRunData.Index], currentRunData.Offset);
                        currentRunData.Index++;

                        // Figure out the ticks until the next rock fires
                        if (currentRunData.Index < mPatternArray.length)
                        {
                            currentRunData.TicksRemaining = mPatternArray[currentRunData.Index].TicksToNext;
                        }
                    }
                }
                else
                {
                    // This one's over
                    mPatternRunData.splice(i, 1);
                    --i;
                }
            }

            super.update();
        }

        /// Fire off a rock
        private function _FireARock(data:RockPatternRockData, offset:Point):void
        {
            var newRock:Rock = mRockAddFunction();
            if (null != newRock)
            {
                // Reset the rock to the position we specify and send it flying
                newRock.Reset();
                newRock.x = data.Offset.x + offset.x;
                newRock.y = data.Offset.y + offset.y;
                newRock.velocity.x = data.Velocity.x;
                newRock.velocity.y = data.Velocity.y;
            }
        }
    }
}