﻿package
{
    import com.adamatomic.flixel.FlxGame;
    import state.*;

    // Either I'm doing something stupid, I'm crazy, or there's a bug.  I told Flixl that the height was 640 and
    // there seems to be ~20 pixels of dead space at the bottom.  So I'm reducing the total height of the Flash
    // magic rectangle by 20 here.
    [SWF(width = "480", height = "620", backgroundColor = "#000000")]
    [Frame(factoryClass="Preloader")]

    public class Main extends FlxGame
    {
        public function Main():void
        {
            //super(480, 640, FEState, 1, 0x000000ff, false);
            //super(480, 640, GameState, 1, 0x000000ff, false);

            // I could turn the Flixel thing off, but I'm kinda proud to have used it.
            super(480, 640, FEState, 1, 0x000000ff, true);
        }
    }
}