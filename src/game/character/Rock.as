﻿package character
{
    import com.adamatomic.flixel.FlxEmitter;
    import com.adamatomic.flixel.FlxSprite;
    import com.adamatomic.flixel.FlxG;

    public class Rock extends FlxSprite
    {
        [Embed(source = "../../data/Rubble.png")] private var RockDestroyImage:Class;
        [Embed(source = "../../data/Rock.png")] private var RockImage:Class;

        /// Emitter for when we blow up
        private var mDestroyedEmitter:FlxEmitter;


        /// When we hit the floor we will die eventually
        private var mDisappearTimer:Number;

        /// We can run the rock in demo mode so it doesn't follow game logic
        public var DemoMode:Boolean;





        public function Rock()
        {
            super(RockImage, 0, 0, false, false);

            // Tweak bounding box
            height = 24;
            width = 24;
            offset.x = 4;
            offset.y = 4;

            Reset();

            mDestroyedEmitter = FlxG.state.add(new FlxEmitter(0, 0, width, height, null, -2, -150, 150, -50, 200, -360, 360, 400, 0, RockDestroyImage, 15, true)) as FlxEmitter;
        }

        /// Reset this rock so it's ready to go again
        public function Reset():void
        {
            exists = true;
            dead = false;

            mDisappearTimer = 120;
            flicker( -1);

            // Either go kinda quickly either left or right
            angularVelocity = Math.random() * 90 - 45;
            angularVelocity += (angularVelocity < 0 ? -90 : 90);


            angle = Math.random() * 360;
        }

        /// The rock ran into a player
        public function OnHitPlayer(p:Player):void
        {
            Explode();
        }

        /// Blow everything up
        public function Explode():void
        {
            mDestroyedEmitter.reset();
            mDestroyedEmitter.x = x;
            mDestroyedEmitter.y = y;

            // Don't go away immediately - stick around so the particle effects don't go away for a bit
            flicker(-1);
            dead = true;
            visible = false;
        }

        /// Override for bouncing off walls
        override public function hitWall():Boolean
        {
            var oldVelX:Number = velocity.x;
            var ret:Boolean = super.hitWall();
            velocity.x = -oldVelX;
            return ret;
        }

        /// When we hit the floor we want to not roll around
        override public function hitFloor():Boolean
        {
            if (!DemoMode)
            {
                velocity.x = 0;
            }
            return super.hitFloor();
        }

        /// Override basic update functionality
        override public function update():void
        {
            if (!DemoMode)
            {
                if (exists)
                {
                    if (!dead)
                    {

                        // Blow up after a time when lying on the ground
                        if (0 == velocity.y)
                        {
                            angularVelocity = 0;
                            mDisappearTimer--
                        }
                        if (0 >= mDisappearTimer)
                        {
                            kill();
                        }
                        else if (60 >= mDisappearTimer && !flickering())
                        {
                            flicker(1);
                        }
                    }
                    else
                    {
                        mDisappearTimer--
                        if (0 >= mDisappearTimer)
                        {
                            kill();
                        }
                    }
                }
            }

            super.update();
        }
    }
}