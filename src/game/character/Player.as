﻿package character
{
    import com.adamatomic.flixel.FlxEmitter;
    import com.adamatomic.flixel.FlxSprite;
    import com.adamatomic.flixel.FlxG;

    public class Player extends FlxSprite
    {
        [Embed(source = "../../data/Dwarf.png")] private var DwarfImage:Class;
        [Embed(source = "../../data/Jump.mp3")] private var JumpSound:Class;
        [Embed(source = "../../data/SmallHit.mp3")] private var SmallHitSound:Class;
        [Embed(source = "../../data/BigHit.mp3")] private var BigHitSound:Class;

        // I was thinking about having flame jets but they didn't add much
        //[Embed(source = "../../data/flamejets.png")] private var FlameJetImage:Class;
        //private var mJumpFire1:FlxEmitter;
        //private var mJumpFire2:FlxEmitter;

        /// Are we in midair doing a sweet jump?
        private var mJumping:Boolean;

        /// How many rocks did we out without touching the ground?
        private var mCurrentCombo:Number = 1;

        /// The jump speed gets crazy when we hit multiple rocks really really close together.
        /// Therefore I'm putting in a jump recharger that gets back to max jump speed pretty quickly.
        /// That way you don't rocket up stupidly when you hit multiple rocks on the floor
        private var mJumpSpeed:Number;
        private const JUMP_RECHARGE_RATE:Number = 40;
        private const STANDING_JUMP_SPEED:Number = 400;
        private const BOUNCING_JUMP_SPEED:Number = 300;

        /// When falling, how much of the downward velocity gets converted into upward
        /// velocity.  Anything remaining gets converted into, uh, heat I guess. :D
        private const FALLING_BOUNCE_CONVERSTION_FACTOR:Number = .5;

        private const EPSILON:Number = .001;

        private const MOVE_ACCELERATION:Number = 20;
        private const DROP_ACCELERATION:Number = 10;
        private const MAX_HORIZONTAL_SPEED:Number = 300;
        private const MAX_VERTICAL_SPEED:Number = 800;
        private const GRAVITY:Number = 600;
        private const HOLDING_JUMP_GRAVITY_REDUCTION:Number = 200;
        private const DRAG:Number = 750;

        /// We can run the player in demo mode so it doesn't follow game logic
        public var DemoMode:Boolean;

        public function Player()
        {
            super(DwarfImage, 0, 0, true, true);

            addAnimation("idle", [0]);
            addAnimation("walk", [1,2,3,2], 10);
            addAnimation("upidle", [4,5,6,5], 10);
            addAnimation("downidle", [7,8,9,8], 10);
            addAnimation("upmove", [10,11,12,11], 10);
            addAnimation("downmove", [13,14,15,14], 10);

            // Change bounding box a little
            height = 8;
            offset.y = 8;

            //mJumpFire1 = FlxG.state.add(new FlxEmitter(0, 0, 3, 0, null, 0.1, -20, 0, 20, 50, -180, 180, 500, 0, FlameJetImage, 15, false)) as FlxEmitter;
            //mJumpFire2 = FlxG.state.add(new FlxEmitter(0, 0, 3, 0, null, 0.1, 0, 20, 20, 50, -180, 180, 500, 0, FlameJetImage, 15, false)) as FlxEmitter;
            //mJumpFire1.active = false;
            //mJumpFire2.active = false;

            maxVelocity.x = MAX_HORIZONTAL_SPEED;
            maxVelocity.y = MAX_VERTICAL_SPEED;
            acceleration.y = GRAVITY;
            drag.x = DRAG;
            mJumpSpeed = STANDING_JUMP_SPEED;
        }

        override public function update():void
        {
            // Make sure our flame jets are at hand
            //mJumpFire1.x = this.x + 2;
            //mJumpFire2.x = this.x + this.width - 2;
            //mJumpFire1.y

            // Can control left and right
            if (_CanMove())
            {
                if(FlxG.kLeft)
                {
                    velocity.x -= MOVE_ACCELERATION;
                }
                else if(FlxG.kRight)
                {
                    velocity.x += MOVE_ACCELERATION;
                }
            }

            // Determine facing based solely on x velocity
            var isMovingHoriz:Boolean = true;
            if (velocity.x > 0)
            {
                facing = RIGHT;
            }
            else if (velocity.x < 0)
            {
                facing = LEFT;
            }
            else
            {
                isMovingHoriz = false;
            }

            // Figure out what animation we should be playing based on other velocities
            var anim:String = "idle"
            if (0 == velocity.y)
            {
                if (isMovingHoriz)
                {
                    anim = "walk";
                }
                else
                {
                    anim = "idle";
                }
            }
            else if (velocity.y > 200)
            {
                if (isMovingHoriz)
                {
                    anim = "downmove";
                }
                else
                {
                    anim = "downidle";
                }
            }
            else
            {
                if (isMovingHoriz)
                {
                    anim = "upmove";
                }
                else
                {
                    anim = "upidle";
                }
            }
            play(anim);

            // Jumpage
            if (FlxG.kUp)
            {
                if (_CanJump())
                {
                    _Jump(STANDING_JUMP_SPEED);
                }
            }

            // Downward, HOOOO
            if (FlxG.kDown)
            {
                if (_CanDrop())
                {
                    velocity.y += DROP_ACCELERATION;
                }
            }

            // While falling, if you hold UP, gravity is reduced!
            if (velocity.y > 0 && FlxG.kUp && !DemoMode)
            {
                acceleration.y = GRAVITY - HOLDING_JUMP_GRAVITY_REDUCTION;
                maxVelocity.y = HOLDING_JUMP_GRAVITY_REDUCTION*2;
            }
            else
            {
                acceleration.y = GRAVITY;
                maxVelocity.y = MAX_VERTICAL_SPEED;
            }

            // Recharge our jumper
            mJumpSpeed = Math.min(BOUNCING_JUMP_SPEED, mJumpSpeed + JUMP_RECHARGE_RATE);

            if (DemoMode)
            {
                // Jump every now and then
                if (GameGlobal.RandomBetween(0, 400) == 0 && onScreen() && velocity.y == 0)
                {
                    _Jump(STANDING_JUMP_SPEED);
                }
            }

            super.update();
        }

        /// Return whether the player can move left and right
        private function _CanMove():Boolean
        {
            //return !mJumping;
            return !DemoMode;
        }

        /// Return whether the player can cause himself to jump
        private function _CanJump():Boolean
        {
            return !mJumping && !DemoMode
        }

        /// Return whether the player can cause himself to move downward faster
        private function _CanDrop():Boolean
        {
            return mJumping && !DemoMode;
        }

        /// Cause the player to launch into the air without abandon
        private function _Jump(speed:Number):void
        {
            //mJumpFire1.active = true;
            //mJumpFire2.active = true;

            mJumping = true;

            // If he's moving down, let's convert some of that downward into upward
            if (velocity.y > 0)
            {
                speed += velocity.y * FALLING_BOUNCE_CONVERSTION_FACTOR;
                velocity.y = 0;
            }

            var volume:Number = speed / MAX_VERTICAL_SPEED;
            FlxG.play(JumpSound, volume);


            velocity.y -= speed;

            mJumpSpeed = 0;
        }

        /// Player overlaps a rock
        public function OnHitRock(rock:Rock):void
        {
            mCurrentCombo++;

            if (!FlxG.kDown)
            {
                FlxG.quake(.01, .03);

                if (FlxG.kUp)
                {
                    _Jump(mJumpSpeed + ((STANDING_JUMP_SPEED - BOUNCING_JUMP_SPEED) * (mJumpSpeed / STANDING_JUMP_SPEED)));
                }
                else
                {
                    _Jump(mJumpSpeed);
                }

                FlxG.play(SmallHitSound);
            }
            else
            {
                FlxG.play(BigHitSound);
            }
        }

        /// The player landed
        override public function hitFloor():Boolean
        {
            mJumping = false;
            mCurrentCombo = 1;

            // Standing jump is always max speed
            mJumpSpeed = BOUNCING_JUMP_SPEED;

            return super.hitFloor();
        }

        /// How big a combo does the player have?
        public function GetCurrentCombo():Number
        {
            return mCurrentCombo;
        }
    }

}
