﻿package info
{
    import com.adamatomic.flixel.FlxText;

    public class PointsText extends FlxText
    {
        private var mTime:Number;

        private const TOTAL_TIME:Number = 60;
        private const TEXT_WIDTH:Number = 40;

        /// Constructor
        public function PointsText()
        {
            super(0, 0, TEXT_WIDTH, 5, "", 0xffffffff, null, 8, "center")
        }

        private function _Reset():void
        {
            mTime = TOTAL_TIME;
            setColor(0x00ffffff);
            exists = true;
            dead = false;
            //Alpha doesn't actually appear to work at all here _tf.alpha = 1;
        }

        /// Set the text on this field along with where it should be
        public function SetValue(text:String, sx:Number, sy:Number):void
        {
            _Reset();
            setText(text);

            x = sx - TEXT_WIDTH/2;    // Account for centering
            y = sy - 5;               // Offset for text size
        }

        /// Update this text
        override public function update():void
        {
            if (exists)
            {
                mTime--;
                y -= .25;

                /*
                 * This doesn't appear to work
                if (mTime < TOTAL_TIME * .5)
                {
                    var alpha:Number = mTime / (TOTAL_TIME * .5);
                    _tf.alpha = alpha;
                }
                */

                if (0 >= mTime)
                {
                    kill();
                }
            }

            super.update();
        }
    }

}