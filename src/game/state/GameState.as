﻿package state
{
    import character.Player;
    import character.Rock;
    import com.adamatomic.flixel.*;
    import flash.geom.Rectangle;
    import info.PointsText;
    import spawner.*;
    import flash.display.MovieClip;
    import mochi.as3.*;


    public class GameState extends FlxState
    {
        [Embed(source = "../../data/Walls.png")] private var WallsImage:Class;
        [Embed(source = "../../data/Floors.png")] private var FloorsImage:Class;
        [Embed(source = "../../data/Cursor.png")] private var CursorImage:Class;
        [Embed(source = "../../data/Choose.mp3")] private var ChooseSound:Class;
        [Embed(source = "../../data/TimeOut.mp3")] private var TimeOutSound:Class;
        [Embed(source = "../../data/TimeRunningOut.mp3")] private var TimeRunningOutSound:Class;
        [Embed(source = "../../data/GameOver.mp3")] private var GameOverSound:Class;


        /// The player object
        private var mPlayer:Player;

        /// Secondary object whose only job is to let the camera can follow it.  We have excellent control
        /// over this object, moreso than the Camera object itself
        private var mCameraTarget:FlxCore;

        /// Array of all the rocks in the game
        private var mRocks:FlxArray;

        /// Array of all the walls in the game
        private var mWalls:FlxArray;

        /// Array of text for when you gain points
        private var mPointsText:FlxArray;

        /// Rectangle covering where exactly all the walls are - used so we can easily arrange things without too many bugs
        private var mWallBounds:Rectangle;

        /// When the next rocks will spawn
        private var mNextRockSpawnTime:Number;

        /// Minimum time to spwan the next rock set
        private var mRockSpawnTimeMin:Number;

        /// Maximum time to spwan the next rock set
        private var mRockSpawnTimeMax:Number;

        /// All the possible rock firing patterns we have available
        private var mRockPatterns:/*RockPattern*/Array;

        /// How many points we have total
        private var mTotalPoints:Number = 0;


        /// Text to show how much time is left in the round
        private var mTimeDisplay:FlxText;

        /// Text to show how many points we have
        private var mPointsDisplay:FlxText;

        /// Text to show what your combo is
        private var mComboDisplay:FlxText;

        /// Text to show what round we're currently on
        private var mCurrentRoundDisplay:FlxText;

        /// Text to show a pre-game tip
        private var mTipDisplay:FlxText;

        /// Text that shows up between rounds to tell you your score
        private var mBetweenRoundTotalScoreDisplay:FlxText;

        /// Text that shows up between rounds to tell you your score - the label for that thing
        private var mBetweenRoundScoreLabelDisplay:FlxText;



        /// Whether the user has asked to restart
        private var mRestartRequested:Boolean;

        /// Button that shows up during GameOver to play again
        private var mRestartButton:FlxButton;

        /// Button to submit score to the leaderboards
        private var mLeaderboardSubmitButton:FlxButton;

        /// Button to just view the leaderboards
        private var mLeaderboardViewButton:FlxButton;

        /// Whether the user already submitted this round's score
        private var mSubmittedThisGame:Boolean = false;



        // A bunch of layers
        private var mRockPatternLayer:FlxLayer;
        private var mWallLayer:FlxLayer;
        private var mRockLayer:FlxLayer;
        private var mPlayerLayer:FlxLayer;
        private var mPointsTextLayer:FlxLayer;
        private var mHudLayer:FlxLayer;



        private const WALL_SIZE:Number = 16;



        /// How much time is left in the current state
        private var mTimer:Number;

        // We'll use the framerate to figure out how many frames these things are exactly
        private const DURING_ROUND_TIME_IN_SECONDS:Number = 45;
        private const PRE_ROUND_TIME_IN_SECONDS:Number = 6;
        private const PRE_ROUND_TIME_ROCK_SPAWN:Number = 2.8;
        private const DURING_ROUND_WARN_TIME_SECONDS:Number = 5;

        /// For debugging I can crank this
        private const STARTING_ROUND:Number = 0;


        /// What round are we on?  Indexed at 0 but we'll +1 for the masses
        private var mCurrentRound:Number = STARTING_ROUND;
        private const TOTAL_NUM_ROUNDS:Number = 6;

        /// We can ask the stage what the framerate is
        private var mFramerate:Number;



        /// Once we know the framerate we can figure out how long the round should last
        private var mTotalDuringRoundTime:Number;

        /// The pre round should take some time also
        private var mTotalPreRoundTime:Number;

        /// We want to start spawning rocks during pre-round to get us right into the action
        private var mTimeAtPreRoundRockSpawn:Number;



        /// State of this GameState - we'll go through each of these basically in order
        private var mCurrentState:Number;
        private const STATE_INITIALIZATION:Number = 0;  ///< Useful because during the constructor, if this is the first State, Flixel hasn't set everything up yet (like "stage")
        private const STATE_PRE_ROUND:Number      = 1;
        private const STATE_DURING_ROUND:Number   = 2;
        private const STATE_POST_ROUND:Number     = 3;
        private const STATE_GAME_OVER:Number      = 4;
        private const STATE_SHUTDOWN:Number       = 5;


        /// Constructor - very little work is done, just prepping for our update loop
        public function GameState()
        {
            super();

            _SetStateTo(STATE_INITIALIZATION);
        }

        /// Override - update every frame
        override public function update():void
        {
            switch (mCurrentState)
            {
                case STATE_INITIALIZATION:  _UpdateInitialize();    break;
                case STATE_PRE_ROUND:       _UpdatePreRound();      break;
                case STATE_DURING_ROUND:    _UpdateDuringRound();   break;
                case STATE_POST_ROUND:      _UpdatePostRound();     break;
                case STATE_GAME_OVER:       _UpdateGameOver();      break;
                case STATE_SHUTDOWN:        _UpdateShutdown();      break;
            }

            super.update();
        }

        /// Change state to the given one
        private function _SetStateTo(newState:Number):void
        {
            // Colin loves state-based logic.
            // Some stuff commented out because I don't need it yet
            switch (mCurrentState)
            {
                //case STATE_INITIALIZATION:  _CleanupInitialize();   break;
                case STATE_PRE_ROUND:       _CleanupPreRound();     break;
                case STATE_DURING_ROUND:    _CleanupDuringRound();  break;
                //case STATE_POST_ROUND:      _CleanupPostRound();    break;
                case STATE_GAME_OVER:       _CleanupGameOver();     break;
                //case STATE_SHUTDOWN:        _CleanupShutdown();     break;
            }
            switch (newState)
            {
                //case STATE_INITIALIZATION:  _SetupInitialize();     break;
                case STATE_PRE_ROUND:       _SetupPreRound();       break;
                case STATE_DURING_ROUND:    _SetupDuringRound();    break;
                //case STATE_POST_ROUND:      _SetupPostRound();      break;
                case STATE_GAME_OVER:       _SetupGameOver();       break;
                //case STATE_SHUTDOWN:        _SetupShutdown();       break;
            }

            mCurrentState = newState;
        }

        /// Run during the initialization state
        private function _UpdateInitialize():void
        {
            FlxG.flash(0xff000000, .75);

            mFramerate = stage.frameRate;
            mTotalDuringRoundTime = mFramerate * DURING_ROUND_TIME_IN_SECONDS;
            mTotalPreRoundTime = mFramerate * PRE_ROUND_TIME_IN_SECONDS;
            mTimeAtPreRoundRockSpawn = mFramerate * PRE_ROUND_TIME_ROCK_SPAWN;

            // Create variables
            mRocks = new FlxArray();
            mWalls = new FlxArray();
            mPointsText = new FlxArray();
            mNextRockSpawnTime = 0;

            // Layers added in a logical order so they render in a nice consistent order
            mWallLayer = add(new FlxLayer()) as FlxLayer;
            mRockLayer = add(new FlxLayer()) as FlxLayer;
            mPlayerLayer = add(new FlxLayer()) as FlxLayer;
            mPointsTextLayer = add(new FlxLayer()) as FlxLayer;
            mHudLayer = add(new FlxLayer()) as FlxLayer;
            mRockPatternLayer = add(new FlxLayer()) as FlxLayer;

            // Walls
            mWallBounds = new Rectangle( 0, 0, FlxG.width, FlxG.height*1.5);
            //mWallBounds = new Rectangle( 0, 0, FlxG.width, FlxG.height);
            mWalls.add(mWallLayer.add(new FlxBlock(mWallBounds.left, mWallBounds.bottom-WALL_SIZE, mWallBounds.width, WALL_SIZE*2, FloorsImage)));
            mWalls.add(mWallLayer.add(new FlxBlock(mWallBounds.left-WALL_SIZE, mWallBounds.top-WALL_SIZE*2, mWallBounds.width+WALL_SIZE*2, WALL_SIZE*2, WallsImage)));
            mWalls.add(mWallLayer.add(new FlxBlock(mWallBounds.left-WALL_SIZE, mWallBounds.top, WALL_SIZE*2, mWallBounds.height, WallsImage)));
            mWalls.add(mWallLayer.add(new FlxBlock(mWallBounds.right - WALL_SIZE, mWallBounds.top, WALL_SIZE*2, mWallBounds.height, WallsImage)));

            // Make the player object
            mPlayer = mPlayerLayer.add(new Player()) as Player;
            _FixPlayerPosition(true);

            // Get the camera to follow the camera target
            mCameraTarget = add(new FlxCore());
            mCameraTarget.x = mPlayer.x;
            mCameraTarget.y = mPlayer.y;
            FlxG.follow(mCameraTarget, 1);
            FlxG.followAdjust(0, .2)
            FlxG.followBounds(mWallBounds.left, mWallBounds.top, mWallBounds.right, mWallBounds.bottom);

            // Set up the HUD
            var hudMidX:Number = FlxG.width / 2;
            var hudMidY:Number = FlxG.height / 2;
            mComboDisplay = mHudLayer.add(new FlxText(20, 0, 100, 20, "", 0xffffff, null, 20, "left")) as FlxText;
            mPointsDisplay = mHudLayer.add(new FlxText(FlxG.width - 220, 0, 200, 20, "0", 0xffffff, null, 16, "right")) as FlxText;
            mTimeDisplay = mHudLayer.add(new FlxText(hudMidX - 100, 0, 200, 20, "", 0xffffff, null, 18, "center")) as FlxText;
            mCurrentRoundDisplay = mHudLayer.add(new FlxText(hudMidX - 150, hudMidY - 200, 300, 20, "", 0xffffff, null, 28, "center")) as FlxText;
            mTipDisplay = mHudLayer.add(new FlxText(hudMidX - 250, hudMidY + 100, 500, 60, "", 0xffffff, null, 12, "center")) as FlxText;
            mBetweenRoundTotalScoreDisplay = mHudLayer.add(new FlxText(hudMidX - 250, hudMidY - 25, 500, 80, "", 0xffffff, null, 28, "center")) as FlxText;
            mBetweenRoundScoreLabelDisplay = mHudLayer.add(new FlxText(hudMidX - 250, hudMidY - 50, 500, 60, "", 0xffffff, null, 12, "center")) as FlxText;

            // Stupid bug alert: Button scrollfactors account for the button images, but do NOT account for the button's
            // mouse hit areas.  Instead of delving into this, I'll just put the restart button in camera space
            mRestartButton = add(new FlxButton(mWallBounds.x + mWallBounds.width / 2 - 80, mWallBounds.bottom - 180,
                new FlxSprite(null, 0, 0, false, false, 160, 20, 0xffffffff),
                OnRestartClicked,
                new FlxSprite(null, 0, 0, false, false, 160, 20, 0xffffffff),
                new FlxText(0, 0, 160, 50, "Play Again", 0x000000, null, 12, "center"),
                new FlxText(0, 0, 160, 50, "Play Again", 0x800000, null, 12, "center"))) as FlxButton;

            // Leaderboards
            mLeaderboardSubmitButton = add(new FlxButton(mWallBounds.x + mWallBounds.width / 2 - 80, mWallBounds.bottom - 240,
                new FlxSprite(null, 0, 0, false, false, 160, 20, 0xffffffff),
                OnSubmitClicked,
                new FlxSprite(null, 0, 0, false, false, 160, 20, 0xffffffff),
                new FlxText(0, 0, 160, 50, "Submit Score", 0x000000, null, 12, "center"),
                new FlxText(0, 0, 160, 50, "Submit Score", 0x800000, null, 12, "center"))) as FlxButton;

            mLeaderboardViewButton = add(new FlxButton(mWallBounds.x + mWallBounds.width / 2 - 80, mWallBounds.bottom - 210,
                new FlxSprite(null, 0, 0, false, false, 160, 20, 0xffffffff),
                OnViewLeaderboardsClicked,
                new FlxSprite(null, 0, 0, false, false, 160, 20, 0xffffffff),
                new FlxText(0, 0, 160, 50, "View Leaderboards", 0x000000, null, 12, "center"),
                new FlxText(0, 0, 160, 50, "View Leaderboards", 0x800000, null, 12, "center"))) as FlxButton;

            // Given time constraints I didn't learn how scrollFactor works yet.
            // For some reason I can't simply do this:
            //     mHudLayer.scrollFactor.y = mHudLayer.scrollFactor.x = 0;
            // I dunno, I have to set it on everything.  Oh well.
            mComboDisplay.scrollFactor.y = mComboDisplay.scrollFactor.x = 0;
            mPointsDisplay.scrollFactor.y = mPointsDisplay.scrollFactor.x = 0;
            mTimeDisplay.scrollFactor.y = mTimeDisplay.scrollFactor.x = 0;
            mCurrentRoundDisplay.scrollFactor.y = mCurrentRoundDisplay.scrollFactor.x = 0;
            mTipDisplay.scrollFactor.y = mTipDisplay.scrollFactor.x = 0;
            // See above bug notes mRestartButton.scrollFactor.y = mRestartButton.scrollFactor.x = 0;
            mBetweenRoundTotalScoreDisplay.scrollFactor.y = mBetweenRoundTotalScoreDisplay.scrollFactor.x = 0;
            mBetweenRoundScoreLabelDisplay.scrollFactor.y = mBetweenRoundScoreLabelDisplay.scrollFactor.x = 0;

            // A few of these things are invisible and/or inactive to start
            mRestartButton.active = false;
            mRestartButton.visible = false;
            mLeaderboardViewButton.active = false;
            mLeaderboardViewButton.visible = false;
            mLeaderboardSubmitButton.active = false;
            mLeaderboardSubmitButton.visible = false;
            mBetweenRoundScoreLabelDisplay.visible = false;
            mBetweenRoundTotalScoreDisplay.visible = false;

            // Set up our leaderboard sos we can see it later
            //var leaderboardMovie:MovieClip = addChild(new MovieClip()) as MovieClip;
            //MochiServices.connect("dedd4ad83aa04e7b", leaderboardMovie);
            GameGlobal.ConnectToLeaderboard(this);

            _SetStateTo(STATE_PRE_ROUND);
        }

        /// Set up the initialize state
        private function _SetupPreRound():void
        {
            mTimer = mTotalPreRoundTime;
            mCurrentRoundDisplay.visible = true;

            if (mCurrentRound < TOTAL_NUM_ROUNDS - 1)
            {
                mCurrentRoundDisplay.setText("Round " + (mCurrentRound + 1));
            }
            else
            {
                mCurrentRoundDisplay.setText("Final Round");
            }

            if (mTotalPoints != 0)
            {
                mBetweenRoundScoreLabelDisplay.setText("Current score:");
                mBetweenRoundTotalScoreDisplay.setText(String(mTotalPoints));
                mBetweenRoundScoreLabelDisplay.visible = true;
                mBetweenRoundTotalScoreDisplay.visible = true;
            }
            mPointsDisplay.visible = false;
            mComboDisplay.visible = false;

            var tip:String = "";
            switch (mCurrentRound)
            {
                case 0:   tip = "Smash rocks for points!";    break;
                case 1:   tip = "Smash rocks without landing to\nimprove your multiplier!";    break;
                case 2:   tip = "Hold DOWN to fall faster\nand smash through rocks!";  break;
                case 3:   tip = "Hold UP to fall slower\nand bounce higher!";    break;
                case 4:   tip = "Rocks near the floor\nare worth more points\n(but don't let them land)!";   break;
                case 5:   tip = "Smash rocks while moving\nfast for more points!";   break;
            }

            mTipDisplay.visible = true;
            mTipDisplay.setText(tip);

            // We have a bunch of possible rock dropping patterns, and those are dependent on round
            _GenerateRockPatterns(mCurrentRound);
        }

        /// Update during the pre round
        private function _UpdatePreRound():void
        {
            _FixPlayerPosition();
            _RunWallCollisions();
            _RunCamera();
            _RunComboTextLogic();

            mTimer--;
            if (0 >= mTimer)
            {
                _SetStateTo(STATE_DURING_ROUND);
            }

            if (mTimeAtPreRoundRockSpawn > mTimer)
            {
                _RunRockSpawning();
            }
        }

        /// Cleanup after the preround state
        private function _CleanupPreRound():void
        {
            mCurrentRoundDisplay.visible = false;
            mTipDisplay.visible = false;
            mBetweenRoundScoreLabelDisplay.visible = false;
            mBetweenRoundTotalScoreDisplay.visible = false;

            mPointsDisplay.visible = true;
            mComboDisplay.visible = true;
        }


        /// Setup before the during round state
        private function _SetupDuringRound():void
        {
            // The first round is going to be a little shorter than the rest
            // because it's so simple and I don't want to drag it when there's
            // so much awesome to come
            if (mCurrentRound < 1)
            {
                mTimer = Math.floor(mTotalDuringRoundTime * 2 / 3);
            }
            else
            {
                mTimer = mTotalDuringRoundTime;
            }


            mTimeDisplay.setColor(0xffffff);
            mTimeDisplay.visible = true;
        }

        /// Update for regular game logic
        private function _UpdateDuringRound():void
        {
            _FixPlayerPosition();
            _RunWallCollisions();
            _RunCamera();
            _RunComboTextLogic();
            _RunRockSpawning();

            // Collisions between player and rocks happen at this time, and points can happen
            FlxG.overlapArray(mRocks, mPlayer, OnPlayerHitRock);

            // Track the time all the time
            mTimer--;
            var timeDisplay:String = _GetTimeText(mTimer);
            mTimeDisplay.setText(timeDisplay);

            // Special stuff to do at special times
            if (0 >= mTimer)
            {
                FlxG.play(TimeOutSound);
                _SetStateTo(STATE_POST_ROUND);
            }
            else if (mTimer <= (mFramerate * DURING_ROUND_WARN_TIME_SECONDS) && (mTimer % mFramerate) == 0)
            {
                FlxG.play(TimeRunningOutSound);

                // Turn the text red with a few seconds left
                if (mTimer == mFramerate * DURING_ROUND_WARN_TIME_SECONDS)
                {
                    mTimeDisplay.setColor(0xff0000);
                }
            }
        }

        /// Cleanup the stuff that happened in the During Round state
        private function _CleanupDuringRound():void
        {
            var i:Number;  // I hate ActionScript 3 ridiculous scoping
            for (i = 0 ; i < mRocks.length ; ++i)
            {
                var rock:Rock = (mRocks[i] as Rock);
                if (rock.exists)
                {
                    rock.Explode();
                }
            }

            for (i = 0 ; i < mRockPatterns.length ; ++i)
            {
                mRockPatterns[i].Douse();
            }

            mTimeDisplay.visible = false;
        }

        /// Update during post round
        private function _UpdatePostRound():void
        {
            _RunWallCollisions();
            _RunCamera();
            _RunComboTextLogic();

            // Not really much to do here.  I was thinking I could do some sort of
            // "you got xx points!" or something.  Turned out to not add much.
            // Leaving this mode here to maybe do it anyway later.
            ++mCurrentRound;
            if (mCurrentRound < TOTAL_NUM_ROUNDS)
            {
                _SetStateTo(STATE_PRE_ROUND);
            }
            else
            {
                _SetStateTo(STATE_GAME_OVER);
            }
        }

        /// Setupup when entering the Game Over state
        private function _SetupGameOver():void
        {
            FlxG.setCursor(CursorImage);
            mRestartButton.active = true;
            mRestartButton.visible = true;
            mLeaderboardSubmitButton.active = true;
            mLeaderboardSubmitButton.visible = true;
            mLeaderboardViewButton.active = true;
            mLeaderboardViewButton.visible = true;
            mCurrentRoundDisplay.setText("Game Over");
            mBetweenRoundScoreLabelDisplay.setText("Final score:");
            mBetweenRoundTotalScoreDisplay.setText(String(mTotalPoints));
            mCurrentRoundDisplay.visible = true;
            mBetweenRoundScoreLabelDisplay.visible = true;
            mBetweenRoundTotalScoreDisplay.visible = true;
            mPointsDisplay.visible = false;
            mComboDisplay.visible = false;
            FlxG.play(GameOverSound);
        }

        /// Update during game over
        private function _UpdateGameOver():void
        {
            _FixPlayerPosition();
            _RunWallCollisions();
            _RunCamera();
            _RunComboTextLogic();

            // Don't show leaderboard buttons if the connection failed
            var leaderboardsConnected:Boolean = MochiServices.connected;
            mLeaderboardViewButton.active = leaderboardsConnected;
            mLeaderboardViewButton.visible = leaderboardsConnected;

            // Also no submitting twice
            mLeaderboardSubmitButton.active = !mSubmittedThisGame && leaderboardsConnected;
            mLeaderboardSubmitButton.visible = !mSubmittedThisGame && leaderboardsConnected;

            if (mRestartRequested)
            {
                mRestartRequested = false;
                mTotalPoints = 0;
                mPointsDisplay.setText("0");
                mCurrentRound = 0;
                _SetStateTo(STATE_PRE_ROUND);
            }
        }

        /// Clean up when leaving the Gmae Over state
        private function _CleanupGameOver():void
        {
            FlxG.setCursor(null);
            mRestartButton.active = false;
            mRestartButton.visible = false;
            mLeaderboardViewButton.active = false;
            mLeaderboardViewButton.visible = false;
            mLeaderboardSubmitButton.active = false;
            mLeaderboardSubmitButton.visible = false;
            mBetweenRoundTotalScoreDisplay.visible = false;
            mBetweenRoundScoreLabelDisplay.visible = false;
            mCurrentRoundDisplay.visible = false;
            mSubmittedThisGame = false;
        }

        /// Update during shutdown
        /// Can't currently get here at all.  Which is fine by me.
        /// Leaving in in case I have time to do a leaderboard or something.
        private function _UpdateShutdown():void
        {
            MochiServices.disconnect();

            FlxG.switchState(FEState);
        }

        /// Rock spawn logic - may need to run during multiple states
        private function _RunRockSpawning():void
        {
            // Spawn rocks over time
            if (0 >= mNextRockSpawnTime)
            {
                // Choose a random pattern and tell it to fire
                if (0 < mRockPatterns.length)
                {
                    var pattern:RockPattern = mRockPatterns[GameGlobal.RandomBetween(0, mRockPatterns.length - 1)];
                    var spawnRectangle:Rectangle = new Rectangle(mWallBounds.left + 50, 0, mWallBounds.width - 100, 0);
                    pattern.Fire(spawnRectangle);
                }

                mNextRockSpawnTime = GameGlobal.RandomBetween(mRockSpawnTimeMin, mRockSpawnTimeMax);
            }
            --mNextRockSpawnTime;
        }

        /// Camera update logic - may need to run during multiple states
        private function _RunCamera():void
        {
            // Move around our camera target to follow the player intelligently
            mCameraTarget.x = mPlayer.x;
            if (mPlayer.velocity.y > 0)
            {
                mCameraTarget.y = Math.min(mPlayer.y + mPlayer.velocity.y * 5, mWallBounds.bottom + 300);
                mCameraTarget.y = Math.min(mPlayer.y + 500, mCameraTarget.y);
            }
            else
            {
                mCameraTarget.y = Math.min(mPlayer.y + mPlayer.velocity.y/2, mWallBounds.bottom);
            }
        }

        /// Run all the collisions with the walls.  We need to do this in multiple modes or we slip through the universe
        private function _RunWallCollisions():void
        {
            FlxG.collideArray(mWalls, mPlayer);
            FlxG.collideArrays(mWalls, mRocks);
        }

        /// Run the logic assocaited with setting the Combo text
        private function _RunComboTextLogic():void
        {
            var combos:Number = mPlayer.GetCurrentCombo();
            if (combos > 1)
            {
                mComboDisplay.setText("x" + combos);
            }
            else
            {
                mComboDisplay.setText("");
            }
        }

        /// The player, he ran into a rock
        private function OnPlayerHitRock(rock:Rock, player:Player):void
        {
            var points:Number = _CalculatePoints(rock, player);
            _GetAvailablePointsText().SetValue(String(points), rock.x + rock.width / 2, rock.y + rock.height / 2);

            mTotalPoints += points;
            mPointsDisplay.setText(String(mTotalPoints));

            player.OnHitRock(rock);
            rock.OnHitPlayer(player);
        }

        /// Figure out how many points the player should get for destroying the given rock
        private function _CalculatePoints(rock:Rock, player:Player):Number
        {
            // Arbitrary algorothm.  I want points to be based on height and vertical speed.
            // Rocks near the bottom that are NOT on the ground are worth the most points

            var height:Number = (rock.y - WALL_SIZE) / 100;

            var speed:Number = Math.abs(player.velocity.y) / 500;

            var multiplier:Number = player.GetCurrentCombo();

            // When moving UP, let's give more points - this is so flying upward through a ton of rocks
            // (which is fun!) provides lots of points
            if (player.velocity.y < 0)
            {
                speed *= 2;
            }

            if (0 != rock.velocity.y)
            {
                return Math.floor(multiplier * (height + speed));
            }
            else
            {
                // Very few points for rocks on the floor - get them RIGHT BEFORE they land for max points
                return multiplier + 1;
            }
        }

        /// Given a number of frames, get the amount of time that represents
        private function _GetTimeText(frames:Number):String
        {
            // The creators of ActionScript 3 may have done many things right,
            // but here's one thing they did horribly wrong.  If I attempt to
            // scope this variable inside each of the below blocks, it will
            // warn me that I am defining the same variable twice.  Which is
            // completely untrue in most other languages where {} scopes variables
            // out so you don't have to worry about crap like this.
            var seconds:Number;
            var secondsStr:String;
            if (frames >= mFramerate * 60)
            {
                var mins:Number = Math.floor(frames / (mFramerate * 60));
                seconds = Math.floor(frames / mFramerate) - (mins * 60);
                secondsStr = (seconds > 9 ? "" : "0") + String(seconds)
                return String(mins) + ":" + secondsStr;
            }
            else if (frames < mFramerate * DURING_ROUND_WARN_TIME_SECONDS)
            {
                seconds = Math.floor(frames / mFramerate);
                var remainder:Number = Math.floor(frames) - seconds * mFramerate;
                var remainderStr:String = (remainder > 9 ? "" : "0") + String(remainder);
                return (seconds == 0 ? "" : String(seconds)) + "." + remainderStr;
            }
            else
            {
                // Just displaying seconds
                seconds = Math.floor(frames / mFramerate);
                secondsStr = String(seconds);
                return secondsStr;
            }
        }

        /// During Game Over the player pressed the Restart button
        private function OnRestartClicked():void
        {
            mRestartRequested = true;
            mRestartButton.active = false;
            FlxG.play(ChooseSound);
        }

        /// Create all the rock patterns we care about
        private function _GenerateRockPatterns(round:Number):void
        {
            mRockPatternLayer.destroy();
            mRockPatterns = new Array();


            // This is a good default but we can override it
            mRockSpawnTimeMin = 200;
            mRockSpawnTimeMax = 200;

            var pattern:RockPattern = null;
            switch (round)
            {
                case 0:
                {
                    pattern = _MakeNewRockPattern();
                    pattern.Add(35, 0, 200, 0);
                    pattern.Add(35, 0, 200, 40);
                    pattern.Add(35, 0, 200, 80);

                    pattern = _MakeNewRockPattern();
                    pattern.Add(35, 0, 200, 0);
                    pattern.Add(35, 0, 200, -40);
                    pattern.Add(35, 0, 200, -80);
                    break;
                }

                case 1:
                {
                    pattern = _MakeNewRockPattern();
                    pattern.Add(50, -40, 250, 0);
                    pattern.Add(50, -40, 250, -40);
                    pattern.Add(50, -40, 250, -80);

                    pattern = _MakeNewRockPattern();
                    pattern.Add(50, 40, 250, 0);
                    pattern.Add(50, 40, 250, 40);
                    pattern.Add(50, 40, 250, 80);

                    mRockSpawnTimeMin = 100;
                    mRockSpawnTimeMax = 100;
                    break;
                }

                case 2:
                {
                    pattern = _MakeNewRockPattern();
                    pattern.Add(25,  100, 200, -100);
                    pattern.Add(25, -100, 200, 100);
                    pattern.Add(25,  100, 200, -50);
                    pattern.Add(25, -100, 200, 50);
                    pattern.Add(25,  100, 200, -0);
                    pattern.Add(25, -100, 200, 0);

                    mRockSpawnTimeMin = 100;
                    mRockSpawnTimeMax = 150;
                    break;
                }

                case 3:
                {
                    pattern = _MakeNewRockPattern();
                    pattern.Add(30, 40, 200, 0);
                    pattern.Add(30, 40, 200, -30);
                    pattern.Add(30, 40, 200, -60);
                    pattern.Add(30, 40, 200, -90);
                    pattern.Add(30, 40, 200, -120);

                    pattern = _MakeNewRockPattern();
                    pattern.Add(30, -40, 200, 0);
                    pattern.Add(30, -40, 200, 30);
                    pattern.Add(30, -40, 200, 60);
                    pattern.Add(30, -40, 200, 90);
                    pattern.Add(30, -40, 200, 120);

                    mRockSpawnTimeMin = 150;
                    mRockSpawnTimeMax = 150;
                    break;
                }

                case 4:
                {
                    pattern = _MakeNewRockPattern();
                    pattern.Add(50, 150, 200, 0);
                    pattern.Add(0, 150, 200, 40);
                    pattern.Add(50, 150, 200, 20);
                    pattern.Add(0, 150, 200, 60);
                    pattern.Add(50, 150, 200, 40);
                    pattern.Add(0, 150, 200, 80);

                    pattern = _MakeNewRockPattern();
                    pattern.Add(50, -150, 200, 0);
                    pattern.Add(0, -150, 200, -40);
                    pattern.Add(50, -150, 200, -20);
                    pattern.Add(0, -150, 200, -60);
                    pattern.Add(50, -150, 200, -40);
                    pattern.Add(0, -150, 200, -80);

                    mRockSpawnTimeMin = 150;
                    mRockSpawnTimeMax = 150;
                    break;
                }

                case 5:
                {
                    pattern = _MakeNewRockPattern();
                    pattern.Add(30, 150, 250, 0);
                    pattern.Add(30, 150, 250, 25);
                    pattern.Add(30, 150, 250, 50);
                    pattern.Add(30, 150, 250, 75);
                    pattern.Add(30, 150, 250, 100);
                    pattern.Add(30, 150, 250, 125);

                    pattern = _MakeNewRockPattern();
                    pattern.Add(30, -150, 250, 0);
                    pattern.Add(30, -150, 250, -25);
                    pattern.Add(30, -150, 250, -50);
                    pattern.Add(30, -150, 250, -75);
                    pattern.Add(30, -150, 250, -100);
                    pattern.Add(30, -150, 250, -125);

                    /*
                     * Neat but provides abnormally many points
                    pattern = _MakeNewRockPattern();
                    pattern.Add(15,  100, 250, -45);
                    pattern.Add(0, -100, 250, 45);
                    pattern.Add(15,  100, 250, -30);
                    pattern.Add(0, -100, 250, 30);
                    pattern.Add(15,  100, 250, -15);
                    pattern.Add(0, -100, 250, 15);
                    pattern.Add(15,  100, 250, 0);
                    pattern.Add(0, -100, 250, 0);
                    */

                    mRockSpawnTimeMin = 75;
                    mRockSpawnTimeMax = 75;
                    break;
                }
            }

            // Call like so:
            //Add(ticksToNext, velocityX, velocityY, offsetX, offsetY)
        }

        /// Make a new pattern and make sure it gets pumped
        private function _MakeNewRockPattern():RockPattern
        {
            var pattern:RockPattern = new RockPattern(_GetAvailableRock);
            mRockPatterns.push(mRockPatternLayer.add(pattern));
            return pattern;
        }

        /// Return an available rock
        private function _GetAvailableRock():Rock
        {
            var newRock:Rock = mRocks.getNonexist() as Rock;
            if (null == newRock)
            {
                if (mRocks.length < 100)
                {
                    newRock = new Rock();
                    mRocks.add(mRockLayer.add(newRock));
                }
            }
            return newRock;
        }

        /// Return an available points text field
        private function _GetAvailablePointsText():PointsText
        {
            var newPoints:PointsText= mPointsText.getNonexist() as PointsText;
            if (null == newPoints)
            {
                if (mPointsText.length < 100)
                {
                    newPoints = new PointsText();
                    mPointsText.add(mPointsTextLayer.add(newPoints));
                }
            }
            return newPoints;
        }

        /// The Player can sometimes get out of bounds.  I saw it happen a few times, but I'm not sure what caused it.
        /// I'm fairly certain I did NOT simply fall through the walls.  It only seems to happen on round end, so I
        /// suspect something to do with lots of particles
        private function _FixPlayerPosition(force:Boolean = false):void
        {
            if (force || mPlayer.x > mWallBounds.width || mPlayer.x < 0 || mPlayer.y > mWallBounds.height || mPlayer.y < -50)
            {
                mPlayer.x = mWallBounds.x + mWallBounds.width / 2;
                mPlayer.y = mWallBounds.bottom - WALL_SIZE - mPlayer.height;
                mPlayer.velocity.x = 0;
                mPlayer.velocity.y = 0;
            }
        }

        /// User wants to submit his score
        private function OnSubmitClicked():void
        {
            mRestartButton.active = false;
            mLeaderboardSubmitButton.active = false;
            mLeaderboardViewButton.active = false;
            mSubmittedThisGame = true;

            MochiScores.showLeaderboard({onClose : OnLeaderboardClosed, boardID: GameGlobal.GetLeaderboardId(), score: mTotalPoints});
        }

        /// User simply wants to view the leaderboards
        private function OnViewLeaderboardsClicked():void
        {
            mRestartButton.active = false;
            mLeaderboardSubmitButton.active = false;
            mLeaderboardViewButton.active = false;

            MochiScores.showLeaderboard({onClose : OnLeaderboardClosed, boardID: GameGlobal.GetLeaderboardId()});
        }

        /// We have left the Mochi leaderboards
        private function OnLeaderboardClosed():void
        {
            mRestartButton.active = true;
            mLeaderboardViewButton.active = true;

            // No submitting twice
            mLeaderboardSubmitButton.active = !mSubmittedThisGame;
            mLeaderboardSubmitButton.visible = !mSubmittedThisGame;
        }
    }
}
