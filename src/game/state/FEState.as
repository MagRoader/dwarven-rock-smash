﻿package state
{
    import character.Player;
    import character.Rock;
    import com.adamatomic.flixel.*;
    import flash.display.MovieClip;
    import mochi.as3.*;

    public class FEState extends FlxState
    {
        private const VERSION_TEXT:String = "v2";


        [Embed(source = "../../data/Cursor.png")] private var CursorImage:Class;
        [Embed(source = "../../data/Choose.mp3")] private var ChooseSound:Class;
        [Embed(source = "../../data/Floors.png")] private var FloorsImage:Class;

        private var mClicked:Boolean;
        private var mFading:Boolean;

        private var mTitleText:FlxText;
        private var mAuthorText:FlxText;
        private var mPlayButton:FlxButton;
        private var mInstructionsButton:FlxButton;
        private var mLeaderboardButton:FlxButton;


        // Demo stuff
        private var mDemoPlayer:Player;
        private var mDemoRock:Rock;
        private var mDemoWalls:FlxArray;

        // If don't know why this didn't work last time I tried to do it, but it totally worked this time!
        private var mLeaderboardActive:Boolean = false;


        /// This class was not intentionally devoid of comments, it just sorta evolved that way.
        public function FEState()
        {
            FlxG.flash(0xff000000, 1.5);

            mTitleText = add(new FlxText(0, 100, FlxG.width, 100, "Dwarven\nRock Smash", 0xffffff, null, 32, "center")) as FlxText;
            mAuthorText = add(new FlxText(0, 180, FlxG.width, 100, "by Colin C.\nmagroader@gmail.com", 0xffffff, null, 10, "center")) as FlxText;
            add(new FlxText(0, 0, FlxG.width, 100, VERSION_TEXT, 0xffffff, null, 10, "right")) as FlxText;


            mPlayButton = add(new FlxButton(
                FlxG.width / 2 - 60,
                FlxG.height - 175,
                new FlxSprite(null, 0, 0, false, false, 120, 20, 0xff000000),
                OnGoClicked,
                new FlxSprite(null, 0, 0, false, false, 120, 20, 0xffffffff),
                new FlxText(0, 0, 120, 50, "Play", 0xffffff, null, 12, "center"),
                new FlxText(0, 0, 120, 50, "Play", 0x000000, null, 12, "center"))) as FlxButton;

            mInstructionsButton = add(new FlxButton(
                FlxG.width / 2 - 60,
                FlxG.height - 150,
                new FlxSprite(null, 0, 0, false, false, 120, 20, 0xff000000),
                OnInstructionsClicked,
                new FlxSprite(null, 0, 0, false, false, 120, 20, 0xffffffff),
                new FlxText(0, 0, 120, 50, "Instructions", 0xffffff, null, 12, "center"),
                new FlxText(0, 0, 120, 50, "Instructions", 0x000000, null, 12, "center"))) as FlxButton;

            mLeaderboardButton = add(new FlxButton(
                FlxG.width / 2 - 60,
                FlxG.height - 125,
                new FlxSprite(null, 0, 0, false, false, 120, 20, 0xff000000),
                OnLeaderboardClicked,
                new FlxSprite(null, 0, 0, false, false, 120, 20, 0xffffffff),
                new FlxText(0, 0, 120, 50, "Leaderboards", 0xffffff, null, 12, "center"),
                new FlxText(0, 0, 120, 50, "Leaderboards", 0x000000, null, 12, "center"))) as FlxButton;

            mLeaderboardButton.active = false;
            mLeaderboardButton.visible = false;


            FlxG.setCursor(CursorImage);

            _SetupDemoScene();
        }

        private function OnGoClicked():void
        {
            mClicked = true;
            mPlayButton.active = false;
            FlxG.play(ChooseSound);
        }

        private function OnInstructionsClicked():void
        {
            mInstructionsButton.kill();
            FlxG.play(ChooseSound);

            add(new FlxText(10, 230, FlxG.width-20, 400,
                "Everyone knows that Dwarves love to dig.\n\nWere you aware that they also love to FLIP OUT?!\n\n\n\nTake control of a Dwarf and smash up all\nthe rocks you can, while looking as\nawesome as you can doing it.\n\nArrow keys move.\n\nPoints are awarded for style!",
                0xffffff, null, 12, "center")) as FlxText;
        }

        /// Disable our other buttons and launch the Mochi leaderboard
        private function OnLeaderboardClicked():void
        {
            mInstructionsButton.active = false;
            mLeaderboardButton.active = false;
            mPlayButton.active = false;
            mLeaderboardActive = true;

            MochiScores.showLeaderboard( { onClose : OnLeaderboardClosed, boardID: GameGlobal.GetLeaderboardId() } );
        }

        /// The leaderboard has closed; enable the buttons again
        private function OnLeaderboardClosed():void
        {
            mLeaderboardActive = false;
            mInstructionsButton.active = true;
            mLeaderboardButton.active = true;
            mPlayButton.active = true;
        }

        private function OnFadeComplete():void
        {
            FlxG.switchState(GameState);
        }

        override public function update():void
        {
            GameGlobal.ConnectToLeaderboard(this);

            if (mClicked && !mFading)
            {
                mFading = true;

                FlxG.fade(0xff000000, 1, OnFadeComplete, true);
            }

            // Show a button if the leaderboards have connected
            if (!mLeaderboardActive)
            {
                var connected:Boolean = MochiServices.connected;
                mLeaderboardButton.active = connected;
                mLeaderboardButton.visible = connected;
            }


            _UpdateDemoScene();


            super.update();
        }

        /// We're gonna put in a little PacMan-like chase scene across the bottom
        private function _SetupDemoScene():void
        {
            mDemoWalls = new FlxArray();

            var BLOCK_SIZE:Number = 16;

            mDemoWalls.add((add(new FlxBlock( -FlxG.width, FlxG.height - BLOCK_SIZE, FlxG.width * 3, BLOCK_SIZE, FloorsImage))));
            mDemoWalls.add((add(new FlxBlock( -FlxG.width, FlxG.height - BLOCK_SIZE * 2, BLOCK_SIZE, BLOCK_SIZE, FloorsImage))));
            mDemoWalls.add((add(new FlxBlock( FlxG.width*2-BLOCK_SIZE, FlxG.height - BLOCK_SIZE*2, BLOCK_SIZE, BLOCK_SIZE, FloorsImage))));

            mDemoPlayer = add(new Player()) as Player;
            mDemoPlayer.x = -400;
            mDemoPlayer.y = FlxG.height - BLOCK_SIZE * 3;
            mDemoPlayer.velocity.x = 100;
            mDemoPlayer.DemoMode = true;
            mDemoPlayer.drag.x = 0;

            mDemoRock = add(new Rock()) as Rock;
            mDemoRock.x = -325;
            mDemoRock.y = FlxG.height - BLOCK_SIZE * 3;
            mDemoRock.velocity.x = 100;
            mDemoRock.DemoMode = true;
            mDemoRock.angularVelocity = 150;
            mDemoRock.acceleration.y = 600;
        }

        /// Tick the demo scene
        private function _UpdateDemoScene():void
        {
            FlxG.collideArray(mDemoWalls, mDemoPlayer);
            FlxG.collideArray(mDemoWalls, mDemoRock);

            _TurnDemoObjectAround(mDemoPlayer);
            _TurnDemoObjectAround(mDemoRock);
        }

        /// See if a demo object needs to be reversed, and if so do it
        private function _TurnDemoObjectAround(obj:FlxSprite):void
        {
            if ((obj.x > FlxG.width * 1.25 && obj.velocity.x > 0) ||
                (obj.x < -FlxG.width * .25 && obj.velocity.x < 0))
            {
                obj.velocity.x = -obj.velocity.x;
                obj.angularVelocity = -obj.angularVelocity;
            }
        }
    }
}